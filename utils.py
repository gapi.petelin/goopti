import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from functools import wraps
import time
import os
import seaborn as sns
from collections import namedtuple
import math

def timeit(func):
    @wraps(func)
    def timeit_wrapper(*args, **kwargs):
        start_time = time.perf_counter()
        result = func(*args, **kwargs)
        end_time = time.perf_counter()
        total_time = end_time - start_time
        print(f'Function {func.__name__} took {total_time:.4f} seconds')
        return result
    return timeit_wrapper

@timeit
def load_data():
    data = pd.read_csv('dataset/data.csv')
    data['srcSoonestPickup'] = pd.to_datetime(data['srcSoonestPickup'])
    data['srcLatestPickup'] = pd.to_datetime(data['srcLatestPickup'])
    data['srcSoonestDropoff'] = pd.to_datetime(data['srcSoonestDropoff'])
    data['srcLatestDropoff'] = pd.to_datetime(data['srcLatestDropoff'])
    data['timeOfPurchase'] = pd.to_datetime(data['timeOfPurchase'])
    return data

@timeit
def average_pickup(df):
    # Create column AveragePickup that is time between LatestPickup and SoonestPickup
    df['AveragePickup'] = df['srcSoonestPickup'] + (df['srcLatestPickup'] - df['srcSoonestPickup']) / 2
    return df

@timeit
def ignore_data_after_2024(df):
    return df.query('AveragePickupRound <= 2024')

@timeit
def point2point_aggregate(all_data):
    # Aggregate data, so it contains counts of passengers between pairs of locations, aggregated into 3-hour buckets
    df = pd.DataFrame()
    df['pickupCity'] = all_data['pickupCity']
    df['dropoffCity'] = all_data['dropoffCity']
    df['Count'] = all_data['numberOfCustomers']
    df['AveragePickupRound'] = all_data['AveragePickup'].apply(lambda x: x.floor('3h') + pd.Timedelta(hours=3))
    return df.groupby(['pickupCity', 'dropoffCity', 'AveragePickupRound']).count().reset_index().sort_values('AveragePickupRound')


def get_location_coordinates_and_ranges(location):
    Location = namedtuple('Location', ['latitude', 'longitude', 'location', 'radius_in_km', 'is_airport'])
    d = {
        "ljubljana": Location(46.056946, 14.505751, 'Ljubljana', 30, False),
        "maribor": Location(46.554650, 15.645881, 'Maribor', 30, False),
        "venice": Location(45.438759, 12.327145, 'Venice', 15, False),
        "marco-polo": Location(45.50528, 12.35194, "Marco-Polo", 2, True),
        "treviso": Location(45.653276, 12.202081, "Treviso", 2, True),
        "ldza": Location(45.74306, 16.06889, "LDZA", 2, True),
        "vie": Location(48.1158333, 16.5665751, 'VIE', 2, True),
        "trs": Location(45.827499, 13.472200, 'TRS', 2, True)
    }
    return d[location.lower()]

@timeit
def fill_missing_with_0(df):
    # Fill in missing data with 0s. This means if no orders were placed between Ljubljana -> Dunaj at 2019-11-06 09:00:00 and that row is missing form the df dataframe, a new row will be created that contains for that route at the time. 
    # It fills out the info that can be infered from data. No entry for specific pair of cities at specific time means 0 reservations were made
    all_dataframes = [df]
    interval = pd.DateOffset(hours=3)
    date_range = pd.date_range(start=df.AveragePickupRound.min(), end=df.AveragePickupRound.max(), freq=interval)
    for pc in df['pickupCity'].unique():
        for dc in df['dropoffCity'].unique():
            if len(df.query(f'(pickupCity=="{pc}" and dropoffCity=="{dc}")'))>0:
                new_df = pd.DataFrame()
                new_df['AveragePickupRound'] = date_range
                new_df['pickupCity'] = pc
                new_df['dropoffCity'] = dc
                new_df['Count'] = 0
                all_dataframes.append(new_df)
    return pd.concat(all_dataframes).groupby(['pickupCity', 'dropoffCity', 'AveragePickupRound']).max().reset_index().sort_values('AveragePickupRound')

def haversine_distance(lat1, lon1, lat2, lon2):
    # Radius of the Earth in kilometers
    earth_radius = 6371.0

    # Convert latitude and longitude from degrees to radians
    lat1_rad = math.radians(lat1)
    lon1_rad = math.radians(lon1)
    lat2_rad = math.radians(lat2)
    lon2_rad = math.radians(lon2)

    # Haversine formula
    dlon = lon2_rad - lon1_rad
    dlat = lat2_rad - lat1_rad
    a = math.sin(dlat / 2)**2 + math.cos(lat1_rad) * math.cos(lat2_rad) * math.sin(dlon / 2)**2
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    distance = earth_radius * c
    return distance

def filter_on_location(df, pickup_location=None, dropoff_location=None):
    # Check if pickup_location is provided and filter on it
    if pickup_location is not None:
        df[f'distance_pickup_{pickup_location.location}'] = df.apply(
            lambda row: haversine_distance(
                pickup_location.latitude, 
                pickup_location.longitude, 
                row['pickupLatitude'], 
                row['pickupLongitude']
            ), axis=1
        )
        df = df[df[f'distance_pickup_{pickup_location.location}'] < pickup_location.radius_in_km]
        df['pickupCity'] = pickup_location.location
    
    # Check if dropoff_location is provided and filter on it
    if dropoff_location is not None:
        df[f'distance_dropoff_{dropoff_location.location}'] = df.apply(
            lambda row: haversine_distance(
                dropoff_location.latitude, 
                dropoff_location.longitude, 
                row['dropoffLatitude'], 
                row['dropoffLongitude']
            ), axis=1
        )
        df = df[df[f'distance_dropoff_{dropoff_location.location}'] < dropoff_location.radius_in_km]
        df['dropoffCity'] = dropoff_location.location
    
    return df
