import numpy as np
import pandas as pd

import logging

import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
from sklearn.neural_network import MLPClassifier
from sklearn import metrics
from sklearn.model_selection import train_test_split, TimeSeriesSplit, cross_val_score
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score, classification_report, confusion_matrix, mean_squared_error
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, mean_absolute_error
from datetime import datetime, timedelta
import math
from sklearn.ensemble import HistGradientBoostingClassifier, HistGradientBoostingRegressor
from sklearn.dummy import DummyClassifier
from sklearn.metrics import roc_auc_score
from catboost import CatBoostClassifier, CatBoostRegressor
from sklearn.ensemble import RandomForestClassifier, RandomForestRegressor
from tqdm import tqdm
from sklearn.neural_network import MLPRegressor
from sklearn.dummy import DummyRegressor
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVR
from sklearn.compose import TransformedTargetRegressor


class FocalLossObjective(object):
    def calc_ders_range(self, approxes, targets, weights):
        # approxes, targets, weights are indexed containers of floats
        # (containers with only __len__ and __getitem__ defined).
        # weights parameter can be None.
        # Returns list of pairs (der1, der2)
        gamma = 2.
        # alpha = 1.
        assert len(approxes) == len(targets)
        if weights is not None:
            assert len(weights) == len(approxes)
        
        exponents = []
        for index in range(len(approxes)):
            exponents.append(math.exp(approxes[index]))

        result = []
        for index in range(len(targets)):
            p = exponents[index] / (1 + exponents[index])

            if targets[index] > 0.0:
                der1 = -((1-p)**(gamma-1))*(gamma * math.log(p) * p + p - 1)/p
                der2 = gamma*((1-p)**gamma)*((gamma*p-1)*math.log(p)+2*(p-1))
            else:
                der1 = (p**(gamma-1)) * (gamma * math.log(1 - p) - p)/(1 - p)
                der2 = p**(gamma-2)*((p*(2*gamma*(p-1)-p))/(p-1)**2 + (gamma-1)*gamma*math.log(1 - p))

            if weights is not None:
                der1 *= weights[index]
                der2 *= weights[index]

            result.append((der1, der2))

        return result
    
def mean_absolute_scaled_error1(y_true, y_pred, y_train):
    y_true0, y_pred0, y_train0 = [], [], []
    for t in y_train:
        if t != 0:
            y_train0.append(t)

    for idx in range(0, len(y_true)):
        if y_true[idx]!=0:
            y_true0.append(y_true[idx])
            y_pred0.append(y_pred[idx])

    return round(mean_absolute_scaled_error(y_true0, y_pred0, y_train0), 4)

def mean_absolute_scaled_error2(y_true, y_pred, y_train):
    y_true0, y_pred0, y_train0 = [], [], []
    for t in y_train:
        if t != 0:
            y_train0.append(t)

    for idx in range(0, len(y_true)):
        if y_true[idx]!=0 or y_pred[idx]!=0:
            y_true0.append(y_true[idx])
            y_pred0.append(y_pred[idx])

    return round(mean_absolute_scaled_error(y_true0, y_pred0, y_train0), 4)

def mean_absolute_scaled_error(y_true, y_pred, y_train):
    average = np.average(y_train)
    mad = mean_absolute_error(y_train, [average] * len(y_train))
    return round(mean_absolute_error(y_true, y_pred) / mad, 4)

datasets = ['LjubljanaMünchen.csv', 'LjubljanaVenice.csv', 'LjubljanaZagreb.csv']

def load_data(files):
    fl = []
    for file in files:
        h = pd.read_csv(file)
        h['Path'] = file
        fl.append(h)
    df = pd.concat(fl).reset_index(drop=True)
    df['Time'] = pd.to_datetime(df['Time'])
    # TODO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1111 FIX CUTOFF DATE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    min_date = pd.to_datetime('2016-1-1')
    df = df[df['Time'] > min_date]
    return df

def create_models(X_train, y_train, model_types='local'):
    import warnings
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        grmodel_lr = TransformedTargetRegressor(regressor=Pipeline([
            ('scaler', StandardScaler()),
            ('linear_regression', LinearRegression(n_jobs=-1))
        ]), func=np.log1p, inverse_func=np.expm1)
        print('linear_regression trained')
        grmodel_lr.fit(X_train, y_train)

        rmodel_mlp = TransformedTargetRegressor(regressor=Pipeline([
            ('scaler', StandardScaler()),
            ('mlp', MLPRegressor())
        ]), func=np.log1p, inverse_func=np.expm1)
        print('mlp_regression trained')
        rmodel_mlp.fit(X_train, y_train)
        
        rmodel_dummy = DummyRegressor(strategy='constant', constant=0)
        rmodel_dummy.fit(X_train, y_train)

        #model_cb = CatBoostRegressor(verbose=False, thread_count=-1)
        model_cb = TransformedTargetRegressor(regressor=Pipeline([
            ('scaler', StandardScaler()),
            ('hgb', HistGradientBoostingRegressor())
        ]), func=np.log1p, inverse_func=np.expm1)
        model_cb.fit(X_train, y_train)
        print('hgb_regression trained')
    
    global_models = [(model_types, 'LR on all values', grmodel_lr), (model_types, 'Zero Predictor', rmodel_dummy), (model_types, 'MLP Regression on all values', rmodel_mlp), (model_types, 'Catboost Regression on all values', model_cb)]
    return global_models


def create_two_fold_models(X_train, y_train, model_types='local'):
    y_train_binary = np.array([1 if val > 0 else 0 for val in y_train])
    
    import warnings
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        
        #classifier = CatBoostClassifier(verbose=False, thread_count=-1, loss_function=FocalLossObjective(), eval_metric="Logloss")
        #classifier.fit(X_train, y_train_binary)
        
        #classifier2 = Pipeline([
        #    ('scaler', StandardScaler()),
        #    ('mlp_regression', MLPClassifier())
        #])
        classifier2 = HistGradientBoostingClassifier()
        classifier2.fit(X_train, y_train_binary)

        reg_X_train = X_train[np.array(y_train_binary) == 1]
        reg_y_train = y_train[np.array(y_train_binary) == 1]

        model_lr = TransformedTargetRegressor(regressor=Pipeline([
            ('scaler', StandardScaler()),
            ('linear_regression', LinearRegression(n_jobs=-1))
        ]), func=np.log1p, inverse_func=np.expm1)
        
        model_lr.fit(reg_X_train, reg_y_train)
        print('linear_regression trained')
        rmodel_mlp = TransformedTargetRegressor(regressor=Pipeline([
            ('scaler', StandardScaler()),
            ('mlp', MLPRegressor())
        ]), func=np.log1p, inverse_func=np.expm1)
        
        rmodel_mlp.fit(reg_X_train, reg_y_train)
        print('mlp_regression trained')

        rmodel_svr = TransformedTargetRegressor(regressor=Pipeline([
            ('scaler', StandardScaler()),
            ('svr', SVR())
        ]), func=np.log1p, inverse_func=np.expm1)
        rmodel_svr.fit(reg_X_train, reg_y_train)
        print('svr_regression trained')

    #local_two_fold_models = [(model_types, 'Classifier + LR', classifier, model_lr), (model_types, 'Classifier + MLP', classifier, rmodel_mlp), (model_types, 'Classifier + SVR', classifier, rmodel_svr)]
    
    local_two_fold_models2 = [(model_types, 'Classifier (HGB) + LR', classifier2, model_lr), (model_types, 'Classifier (HGB) + MLP', classifier2, rmodel_mlp), (model_types, 'Classifier (HGB) + SVR', classifier2, rmodel_svr)] 
    
    local_two_fold_models = []
    return local_two_fold_models + local_two_fold_models2
    
df = load_data(datasets)


data = {'name': [], 'type': [], 'path': [], 'pred': [], 'true': [], 'reg_raw': [], 'class_raw': [], 'class_prob': []}

start_date = pd.to_datetime('2019-12-02')
end_date = pd.to_datetime('2019-12-31')
test_duration = pd.Timedelta(days=1)

for test_end_date in pd.date_range(start_date, end_date, freq='1D'):
    test_start_date = test_end_date - test_duration
    train_data = df[df['Time'] < test_start_date]
    test_data = df[(df['Time'] >= test_start_date) & (df['Time'] < test_end_date)]
    
    print("Train period:", train_data['Time'].min(), "to", train_data['Time'].max(), "Test period:", test_data['Time'].min(), "to", test_data['Time'].max(), "Train size:", len(train_data), "Test size:", len(test_data))
    
    ignore_column = ['Time', 'Demand', 'Path']
    
    X_train = train_data[train_data.columns.difference(ignore_column)]
    y_train = train_data['Demand']
    
    # Create global models
    global_models = create_models(X_train, y_train, model_types='global')
    global_two_fold_models = create_two_fold_models(X_train, y_train, model_types='global')
    
    
    
    for file in datasets:
        X_train = train_data.query(f'Path=="{file}"')[train_data.columns.difference(ignore_column)]
        y_train = train_data.query(f'Path=="{file}"')['Demand']
        
        # Create global models
        local_models = create_models(X_train, y_train, model_types='local')
        local_two_fold_models = create_two_fold_models(X_train, y_train, model_types='local')
        
        
        X_test = test_data.query(f'Path=="{file}"')[test_data.columns.difference(ignore_column)]
        y_test = test_data.query(f'Path=="{file}"')['Demand']
        y_test_binary = np.array([1 if val > 0 else 0 for val in y_test])
        
        for model_type, model_name, model in global_models + local_models:
            y_pred = model.predict(X_test)
            
            data['name'].extend([model_name]*len(y_pred))
            data['path'].extend([file]*len(y_pred))
            data['type'].extend([model_type]*len(y_pred))
            data['pred'].extend(np.round(y_pred))
            data['true'].extend(np.array(y_test))
            
            data['reg_raw'].extend(np.array(y_pred))
            data['class_raw'].extend(np.array([-1]*len(y_pred)))
            data['class_prob'].extend(np.clip(y_pred, 0, 1))

        for model_type, model_name, modelc, modelr in global_two_fold_models + local_two_fold_models:
            y_pred_c = modelc.predict(X_test)
            y_pred_r = y_pred_r = np.clip(modelr.predict(X_test), 1, None)

            # Merge both predictions
            y_pred = y_pred_r *y_pred_c
            
            data['name'].extend([model_name]*len(y_pred))
            data['path'].extend([file]*len(y_pred))
            data['type'].extend([model_type]*len(y_pred))
            data['pred'].extend(np.round(y_pred))
            data['true'].extend(np.array(y_test))
            
            data['reg_raw'].extend(np.array(y_pred_r))
            data['class_raw'].extend(np.array(y_pred_c))
            data['class_prob'].extend(modelc.predict_proba(X_test)[:, 1].flatten())
            
            
stats = {'name': [], 'type': [], 'path': [], 'mase1': [], 'mase2': [], 'auc': [], 'auc2': []}

dfdata = pd.DataFrame(data)
for model in dfdata.name.unique():
    for typ in dfdata.type.unique():
        for path in dfdata.path.unique():
            v = dfdata.query(f'name=="{model}" and type=="{typ}" and path=="{path}"')
            b_pred = np.where(v.pred < 0.5, 0, 1)
            b_true = np.where(v.true < 0.5, 0, 1)
            b_pred_prob = np.clip(np.array(v.class_prob), 0, 1)
            
            
            auc = roc_auc_score(b_true, b_pred)
            auc2 = roc_auc_score(b_true, b_pred_prob)
            
            mase1 = mean_absolute_scaled_error1(np.array(v.true), np.array(v.pred), np.array(df[df['Time'] < test_start_date]['Demand']))
            mase2 = mean_absolute_scaled_error2(np.array(v.true), np.array(v.pred), np.array(df[df['Time'] < test_start_date]['Demand']))
            
            stats['name'].append(model)
            stats['auc'].append(auc)
            stats['auc2'].append(auc2)
            stats['mase1'].append(mase1)
            stats['mase2'].append(mase2)
            stats['path'].append(path)
            stats['type'].append(typ)

import random
random_int = random.randint(0, 100000)

preds = pd.DataFrame(data)
preds.to_csv(f'preds_{random_int}.csv', index=False)
stat_save = pd.DataFrame(stats).sort_values(['name', 'type', 'path'])
stat_save.to_csv(f'stat_save_{random_int}.csv', index=False)